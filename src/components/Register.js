import React, {useState} from 'react'
import { Button, Form, Row, Col } from 'react-bootstrap'
import formControlFields from "./formControlFields";
import validate from './FormValidationRules';

const Register = () => {

    const register = () => {
        console.log('User Registered Successfully!');
    }
    const {values, errors, handleChange, handleSubmit} = formControlFields(register, validate);
    
    return (
        <Form onSubmit={handleSubmit} noValidate>
            <h2 className="text-center">Sign Up for Free</h2>
            <Form.Row>
                <Form.Group as={Col} xs="6" controlId="firstName">
                    <Form.Control
                        required
                        type="text"
                        name="firstName"
                        placeholder="First name"
                        onChange={handleChange} 
                        value={values.firstName || ''} 
                        className={`${errors.firstName && 'is-error'}`}/>
                </Form.Group>
                <Form.Group as={Col} xs="6" controlId="lastName">
                    <Form.Control
                        required
                        type="text"
                        name="lastName"
                        placeholder="Last name"
                        onChange={handleChange} 
                        value={values.lastName || ''}
                        className={`${errors.lastName && 'is-error'}`}/>
                </Form.Group>
                <Form.Group as={Col} xs="12" controlId="email">
                    <Form.Control
                        required
                        type="email"
                        name="email"
                        placeholder="Email Address"
                        onChange={handleChange} 
                        value={values.email || ''}
                        className={`${errors.email && 'is-error'}`}/>
                </Form.Group>
                <Form.Group as={Col} xs="12" controlId="password">
                    <Form.Control
                        required
                        type="password"
                        name="password"
                        placeholder="Set a Password"
                        onChange={handleChange} 
                        value={values.password || ''}
                        className={`${errors.password && 'is-error'}`}/>
                </Form.Group>
            </Form.Row>
            <Button type="submit" variant="secondary" block>Get Started</Button>
        </Form>
    )
}

export default Register
