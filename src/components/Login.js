import React, {useState} from 'react'
import { Button, Form, Row, Col } from 'react-bootstrap'
import formControlFields from "./formControlFields";
import validate from './FormValidationRules';

const Login = () => {

    const login = () => {
    }
    const {values, errors, handleChange, handleSubmit} = formControlFields(login, validate);
    
    return (
        <Form onSubmit={handleSubmit} noValidate>
            <h2 className="text-center">Welcome Back!</h2>
            <Form.Row>
                <Form.Group as={Col} xs="12" controlId="email">
                    <Form.Control
                        required
                        type="email"
                        name="email"
                        placeholder="Email Address"
                        onChange={handleChange} 
                        value={values.email || ''}
                        className={`${errors.email && 'is-error'}`}/>
                </Form.Group>
                <Form.Group as={Col} xs="12" controlId="password">
                    <Form.Control
                        required
                        type="password"
                        name="password"
                        placeholder="Set a Password"
                        onChange={handleChange} 
                        value={values.password || ''}
                        className={`${errors.password && 'is-error'}`}/>
                </Form.Group>
            </Form.Row>
            <Button type="submit" variant="secondary" block>Log in</Button>
        </Form>
    )
}

export default Login
