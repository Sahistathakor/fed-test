import React, {useState} from 'react'
import { Tabs, Tab, Button, Form, Row, Col } from 'react-bootstrap'
import Login from './Login';
import Register from './Register';

const LoginSignup = () => {
    const [key, setKey] = useState('register');
    return (
        <div className="box-wrapper">
            <Tabs
                id="controlled-tab-example"
                activeKey={key}
                onSelect={(k) => setKey(k)}
                >
                <Tab eventKey="register" title="Register">
                    <div className="tabContent">
                        <Register />
                    </div>
                </Tab>
                <Tab eventKey="login" title="Login">
                    <div className="tabContent">
                        <Login />
                    </div>
                </Tab>
            </Tabs>
        </div>
    )
}

export default LoginSignup
