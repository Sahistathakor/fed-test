import { Container, Row, Col } from 'react-bootstrap';
import LoginSignup from './components/LoginSignup';
import logo from './logo.svg';

function App() {
  return (
    <Container className="main">
        <div className="main-wrapper">
          <Row className="align-items-center justify-content-center">
            <Col md={6}>
              <LoginSignup />
            </Col>
          </Row>
        </div>
    </Container>
  );
}

export default App;
